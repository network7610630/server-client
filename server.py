import socket

def main():
    host = '0.0.0.0'
    port = 12345

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((host, port))
    server_socket.listen(1)
    print("Server listening on port 12345")

    connection, client_address = server_socket.accept()
    print("Connected to:", client_address)

    filename = 'a.txt'
    try:
        with open(filename, 'r') as file:
            file_content = file.read()
            connection.send(file_content.encode())
    except Exception as e:
        print("Error sending file content:", e)


    connection.close()
    print("close socket")
    server_socket.close()


if __name__ == "__main__":
    main()