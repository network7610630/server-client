import socket

def receive():

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect(('angsila.informatics.buu.ac.th', 12345))

    received_data = b""
    while True:
        data = client_socket.recv(1024)
        if not data:
            break
        received_data += data

    received_text = received_data.decode("utf-8")
    print("File received and saved as 'received.txt'")
    print(received_text)

    client_socket.close()
   
if __name__ == "__main__":
    receive()